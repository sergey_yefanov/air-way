#import "GlobalImport.h"

@interface Contentpage : UIViewController <UIDocumentInteractionControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    IBOutlet UIButton           * backButton;
    IBOutlet UITableView        * contentTable;
    NSMutableArray              * pathToDir;
    int                         dirLevel;
    NSMutableString             * shared_filePath;
    NSMutableString             * shared_fileName;
    IBOutlet UILabel            * currentFolder;
    IBOutlet UILabel            * contentSpaceLabel;
    IBOutlet UIProgressView     * contentSpaceProgress;
    BOOL                        mayUpdateProgress;
    IBOutlet UILabel            * noFiles;
    IBOutlet UIButton           * newFolderButton;
    UITextField                 * newNameField;
    NSMutableString             * shared_filePath_Rename;
    IBOutlet UIView             * importPhotosView;
    IBOutlet UIButton           * importPhotosButton;
}

- (IBAction)backInTree:(id)sender;
- (IBAction)closeContentPage:(id)sender;
- (IBAction)enterEditMode:(id)sender;
- (IBAction)addNewFolder:(id)sender;
- (IBAction)importPhotos:(id)sender;

@end