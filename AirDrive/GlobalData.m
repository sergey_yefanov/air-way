#import "GlobalData.h"

@implementation GlobalData

@synthesize local_ip_text;
@synthesize web_ip_text;
@synthesize wifiProgress;
@synthesize httpServer;
@synthesize contentLibraryArray;
@synthesize authPassword;

@synthesize bluetoothPicker;
@synthesize bluetoothPeers;
@synthesize bluetoothSession;
@synthesize bluetoothFileSessionNumber;
@synthesize bluetoothIndicator;
@synthesize bluetoothProgress;

@synthesize FTPServer;
@synthesize connectionPort;

static GlobalData * sharedGlobalData = nil;

+ (GlobalData *)sharedGlobalData 
{
    if (sharedGlobalData == nil) 
    {
        sharedGlobalData = [[super allocWithZone:nil] init];
        sharedGlobalData.local_ip_text = [[[NSMutableString alloc] init] autorelease];
        sharedGlobalData.web_ip_text = [[[NSMutableString alloc] init] autorelease];
        sharedGlobalData.authPassword = [[[NSMutableString alloc] init] autorelease];
        sharedGlobalData.contentLibraryArray = [[[NSMutableArray alloc] init] autorelease];
        
        sharedGlobalData.bluetoothPeers = [[[NSMutableArray alloc] init] autorelease];

        [sharedGlobalData.local_ip_text setString:NSLocalizedString(@"нет соединения", nil)];
        [sharedGlobalData.web_ip_text setString:NSLocalizedString(@"нет соединения", nil)];
        [sharedGlobalData.authPassword setString:@""];
        sharedGlobalData.wifiProgress = 0;
        
        sharedGlobalData.connectionPort = [[[NSMutableString alloc] init] autorelease];
        [sharedGlobalData.connectionPort setString:@"8080"];
        
        NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
        if ([settings stringForKey:@"authPassword"]) [sharedGlobalData.authPassword setString:[settings stringForKey:@"authPassword"]];
        if ([settings stringForKey:@"connectionPort"]) [sharedGlobalData.connectionPort setString:[settings stringForKey:@"connectionPort"]];
        int port;
        @try 
        {
            port = [sharedGlobalData.connectionPort intValue];
            if (port < 1024)
            {
                port = 8080;
            }
            if (port > 32768)
            {
                port = 8080;
            }
            [sharedGlobalData.connectionPort setString:[NSString stringWithFormat:@"%d", port]];
        }
        @catch (NSException * exception) 
        {
            [sharedGlobalData.connectionPort setString:@"8080"];
        }
        @finally 
        {
            //
        }
        
        sharedGlobalData.httpServer = [HTTPServer new];
        [sharedGlobalData.httpServer setType:@"_http._tcp."];
        [sharedGlobalData.httpServer setConnectionClass:[MyHTTPConnection class]];
        [sharedGlobalData.httpServer setDocumentRoot:[NSURL fileURLWithPath:[VKontakteWorks getRootDir]]];
    }
    return sharedGlobalData;
}

+ (id)allocWithZone:(NSZone *)zone 
{
	@synchronized(self)
	{
		if (sharedGlobalData == nil)
		{
			sharedGlobalData = [super allocWithZone:zone];
			return sharedGlobalData;
		}
	}
	return nil;
}

- (id)copyWithZone:(NSZone *)zone 
{
    return self;
}

- (id)retain 
{
    return self;
}

- (NSUInteger)retainCount 
{
    return NSUIntegerMax;
}

- (id)autorelease 
{
    return self;
}

@end