#import "GlobalImport.h"

@interface ViewController : UIViewController <GKPeerPickerControllerDelegate, GKSessionDelegate>
{
    IBOutlet UIImageView    * kolpak_zad;
    IBOutlet UIImageView    * kolpak_pered;
    IBOutlet UIImageView    * indicator;
    IBOutlet UIImageView    * indicator_red;
    IBOutlet UIImageView    * switcher;
    IBOutlet UIButton       * switchButton;
    IBOutlet UILabel        * hintLocal;
    IBOutlet UILabel        * hintWeb;
    IBOutlet UILabel        * localIpText;
    IBOutlet UILabel        * localIpText_ftp;
    IBOutlet UILabel        * webIpText;
    IBOutlet UILabel        * webIpText_ftp;
    IBOutlet UIProgressView * fileUploadProgress;
    IBOutlet UIScrollView   * scroller;
    IBOutlet UIPageControl  * pager;
    IBOutlet UIImageView    * splash_old;
    
    BOOL                    serverIsOn;
    BOOL                    isFlashingError;
}

- (IBAction)infoButtonPressed:(id)sender;
- (IBAction)switchButtonPressed:(id)sender;
- (IBAction)contentButtonPressed:(id)sender;
- (IBAction)setupButtonPressed:(id)sender;

- (IBAction)howToConnect_localMac:(id)sender;
- (IBAction)howToConnect_localWin:(id)sender;
- (IBAction)howToConnect_webMac:(id)sender;
- (IBAction)howToConnect_webWin:(id)sender;

@end