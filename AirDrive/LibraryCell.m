#import "LibraryCell.h"

@implementation LibraryCell

@synthesize elementName;
@synthesize elementDetails;
@synthesize elementImage;
@synthesize editButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
    if (selected == true) 
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [upperShadow setAlpha:0.2];
                             [lowerShadow setAlpha:0.2];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
    }
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [upperShadow setAlpha:0.0];
                             [lowerShadow setAlpha:0.0];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    if (editing == true) 
    {
        [self setAccessoryType:UITableViewCellAccessoryNone];
        [editButton setHidden:false];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [editButton setAlpha:1.0];
                             [elementName setFrame:CGRectMake(60, 11, 200, 20)];
                             [elementDetails setFrame:CGRectMake(60, 31, 200, 20)];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
    }
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [editButton setAlpha:0.0];
                             if (self.tag == 1)
                             {
                                 [elementName setFrame:CGRectMake(60, 11, 230, 20)];
                                 [elementDetails setFrame:CGRectMake(60, 31, 230, 20)];
                             }
                             else
                             {
                                 [elementName setFrame:CGRectMake(60, 11, 250, 20)];
                                 [elementDetails setFrame:CGRectMake(60, 31, 250, 20)];
                             }
                         } 
                         completion:^(BOOL finished){
                             [editButton setHidden:true];
                             if (self.tag == 1) [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
                         }
         ];
    }
}

@end