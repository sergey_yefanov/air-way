//
//  This class was created by Nonnus,
//  who graciously decided to share it with the CocoaHTTPServer community.
//

#import "localhostAddresses.h"

#import <ifaddrs.h>
#import <netinet/in.h>
#import <sys/socket.h>

#import "GlobalData.h"

@implementation localhostAddresses

+ (void)list
{
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	NSMutableDictionary * result = [NSMutableDictionary dictionary];
	struct ifaddrs * addrs;
	BOOL success = (getifaddrs(&addrs) == 0);
	if (success) 
	{
		const struct ifaddrs * cursor = addrs;
		while (cursor != NULL) 
		{
			NSMutableString * ip;
			if (cursor->ifa_addr->sa_family == AF_INET) 
			{
				const struct sockaddr_in* dlAddr = (const struct sockaddr_in*)cursor->ifa_addr;
				const uint8_t * base = (const uint8_t *)&dlAddr->sin_addr;
				ip = [[NSMutableString new] autorelease];
				for (int i = 0; i < 4; i++) 
				{
					if (i != 0) 
						[ip appendFormat:@"."];
					[ip appendFormat:@"%d", base[i]];
				}
				[result setObject:(NSString *)ip forKey:[NSString stringWithFormat:@"%s", cursor->ifa_name]];
			}
			cursor = cursor->ifa_next;
		}
		freeifaddrs(addrs);
	}

    NSMutableURLRequest * request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:@"http://corz.org/ip"]];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:5.0];
    NSData * returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString * results;
    results = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if (results) [result setObject:results forKey:@"www"];
    [results release];
    
	NSLog(@"IP addresses: %@", result);

    NSString * localIP = [result objectForKey:@"en0"];
    NSString * wwwIP = [result objectForKey:@"www"];
    
	if (!localIP) localIP = [result objectForKey:@"en1"];
    
	if (!localIP)
    {
		[[GlobalData sharedGlobalData].local_ip_text setString:@""];
    }
	else
    {
        //NSString * first3Chars = [localIP substringToIndex:3];
        //if ([first3Chars isEqualToString:@"10."] == true)
        //{
        //    [[GlobalData sharedGlobalData].local_ip_text setString:@""];
        //}
        //else
        {
            [[GlobalData sharedGlobalData].local_ip_text setString:localIP];
        }
    }
    
	if (!wwwIP)
    {
        [[GlobalData sharedGlobalData].web_ip_text setString:@""];
    }
	else
    {
        if ([wwwIP isEqualToString:@"Too frequent!"] == true)
        {
            [[GlobalData sharedGlobalData].web_ip_text setString:@""];
        }
        else if ([wwwIP rangeOfString:@"<"].length > 0)
        {
            [[GlobalData sharedGlobalData].web_ip_text setString:@""];
        }
        else
		[[GlobalData sharedGlobalData].web_ip_text setString:wwwIP];
    }

	[pool release];
}

@end
