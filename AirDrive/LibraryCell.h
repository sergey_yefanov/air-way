#import "GlobalImport.h"

@interface LibraryCell : UITableViewCell
{
    IBOutlet UILabel        * elementName;
    IBOutlet UILabel        * elementDetails;
    IBOutlet UIImageView    * elementImage;
    IBOutlet UIButton       * editButton;
    IBOutlet UIView         * upperShadow;
    IBOutlet UIView         * lowerShadow;
}

@property (nonatomic, retain) UILabel * elementName;
@property (nonatomic, retain) UILabel * elementDetails;
@property (nonatomic, retain) UIImageView * elementImage;
@property (nonatomic, retain) UIButton * editButton;

@end