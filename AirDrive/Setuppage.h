#import "GlobalImport.h"

@interface Setuppage : UIViewController
{
    IBOutlet UITextField    * authField;
    IBOutlet UILabel        * authHeader;
    IBOutlet UITextField    * portField;
    IBOutlet UILabel        * portHeader;
    IBOutlet UIImageView    * btSwitcher;
    IBOutlet UIButton       * btSwitchButton;
    IBOutlet UILabel        * btHeader;
    IBOutlet UILabel        * btDescription;
}

- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)textFieldReturn:(id)sender;
- (IBAction)btSwitchButtonPressed:(id)sender;

@end