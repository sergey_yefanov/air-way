#import "AppDelegate.h"
#import "ViewController.h"
#import "GlobalImport.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    [[UIApplication sharedApplication] setIdleTimerDisabled:true];
    [[UIApplication sharedApplication] setStatusBarHidden:false withAnimation:UIStatusBarAnimationSlide];
    
    //AVAudioSession * audioSession = [AVAudioSession sharedInstance];
    //[audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    //[audioSession setActive:true error:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if ((url != nil) && [url isFileURL])
    {
        NSString * fileName = [[url absoluteString] lastPathComponent];
        fileName = [fileName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        fileName = [fileName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

        NSString * saveDirImported = [[VKontakteWorks getRootDir] stringByAppendingPathComponent:NSLocalizedString(@"Импортированные", nil)];

        if ([[NSFileManager defaultManager] fileExistsAtPath:saveDirImported] == false)
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:saveDirImported withIntermediateDirectories:false attributes:nil error:nil];
        }
        
        NSString * destinationFilePath = [saveDirImported stringByAppendingPathComponent:fileName];
        NSURL * destinationUrl = [NSURL fileURLWithPath:destinationFilePath];
        [[NSFileManager defaultManager] copyItemAtURL:url toURL:destinationUrl error:nil];

        NSString * inboxDir = [VKontakteWorks getInboxDir];
        if ([[NSFileManager defaultManager] fileExistsAtPath:inboxDir] == true)
        {
            [[NSFileManager defaultManager] removeItemAtPath:inboxDir error:nil];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            NSString * mes = [NSString stringWithFormat:NSLocalizedString(@"Файл %@ успешно импортирован", nil), fileName];
            UIAlertView * importAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Уведомление", nil) message:mes delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
            [importAlert show];
            [importAlert release];
        });
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
    }    
    return true;
}

@end