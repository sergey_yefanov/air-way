#import "ViewController.h"
#import "GlobalImport.h"
#import "Infopage.h"
#import "Contentpage.h"
#import "Setuppage.h"
#import "localhostAddresses.h"

@implementation ViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    const char * filePath = [[URL path] fileSystemRepresentation];
    const char * attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

- (void)setDoNotBackup
{
    @try
    {
        NSString * rootDir = [VKontakteWorks getRootDir];
        NSArray * filesInRootDir = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:rootDir error:nil];
        [self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:rootDir]];
        for (int i = 0; i < [filesInRootDir count]; i++)
        {
            NSString * fileInDocumentsFolder_Short = [filesInRootDir objectAtIndex:i];
            NSString * fileInDocumentsFolder = [rootDir stringByAppendingPathComponent:fileInDocumentsFolder_Short];
            if ([self addSkipBackupAttributeToItemAtURL:[NSURL URLWithString:fileInDocumentsFolder]] == true)
            {
                //NSLog(@"Do not backup \"%@\": TRUE", fileInDocumentsFolder_Short);
            }
            else
            {
                //NSLog(@"Do not backup \"%@\": FALSE", fileInDocumentsFolder_Short);
            }
        }
    }
    @catch (NSException * exception)
    {
        NSLog(@"Ошибка при установке аттрибутов!");
    }
    @finally
    {
        //
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [scroller setContentSize:CGSizeMake(640, 60)];
    serverIsOn = false;
    isFlashingError = false;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateWiFiProgressBar)
                                                 name:@"updateWiFiProgressBar"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(authError)
                                                 name:@"authError"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(switchServerOFF)
                                                 name:@"switchServerOFF"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setDoNotBackup)
                                                 name:@"setDoNotBackup"
                                               object:nil];
    
    [hintLocal setText:NSLocalizedString(@"как соединиться:", nil)];
    [hintWeb setText:NSLocalizedString(@"как соединиться:", nil)];
    [localIpText setText:NSLocalizedString(@"нет соединения", nil)];
    [localIpText_ftp setText:NSLocalizedString(@"нет соединения", nil)];
    [webIpText setText:NSLocalizedString(@"нет соединения", nil)];
    [webIpText_ftp setText:NSLocalizedString(@"нет соединения", nil)];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationTransitionNone 
                     animations:^{
                         [splash_old setAlpha:0.0];
                     }
                     completion:^(BOOL finished){
                         [splash_old setHidden:true];
                     }
     ];
    
    [GlobalData sharedGlobalData].bluetoothPicker = [[GKPeerPickerController alloc] init];
    // Не забудь сделать релиз в Unload'е
    [GlobalData sharedGlobalData].bluetoothPicker.connectionTypesMask = GKPeerPickerConnectionTypeNearby;
    [GlobalData sharedGlobalData].bluetoothPicker.delegate = self;
    [GlobalData sharedGlobalData].bluetoothFileSessionNumber = -1;
    [GlobalData sharedGlobalData].bluetoothIndicator = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Передача файла:", nil) message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [GlobalData sharedGlobalData].bluetoothProgress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [[GlobalData sharedGlobalData].bluetoothProgress setFrame:CGRectMake(31.0, 65.0, 222.0, 9.0)];
    [[GlobalData sharedGlobalData].bluetoothIndicator addSubview:[GlobalData sharedGlobalData].bluetoothProgress];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setDoNotBackup" object:nil];
}

- (void)updateWiFiProgressBarBG
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    @try 
    {
        [fileUploadProgress setProgress:[GlobalData sharedGlobalData].wifiProgress];
        if (([fileUploadProgress progress] == 0) || ([fileUploadProgress progress] == 100)) 
        {
            if ([fileUploadProgress isHidden] == false)
            {
                [fileUploadProgress setHidden:true];
            }
        }
        else
        {
            if ([fileUploadProgress isHidden] == true)
            {
                [fileUploadProgress setHidden:false];
            }
        }
    }
    @catch (NSException * exception)
    {
        //
    }
    @finally
    {
        [pool release];
    }
}

- (void)updateWiFiProgressBar
{
    [self performSelectorInBackground:@selector(updateWiFiProgressBarBG) withObject:nil];
}

- (void)authError
{
    if (isFlashingError == false) 
    {
        isFlashingError = true;

        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationTransitionNone 
                         animations:^{ [indicator setAlpha:0.0]; } 
                         completion:^(BOOL finished)
        { 
            [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationTransitionNone 
                             animations:^{ [indicator_red setAlpha:1.0]; } 
                             completion:^(BOOL finished)
             { 
                 [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationTransitionNone 
                                  animations:^{ [indicator_red setAlpha:0.0]; } 
                                  completion:^(BOOL finished)
                  { 
                      [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationTransitionNone 
                                       animations:^{ [indicator_red setAlpha:1.0]; } 
                                       completion:^(BOOL finished)
                       { 
                           [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationTransitionNone 
                                            animations:^{ [indicator_red setAlpha:0.0]; } 
                                            completion:^(BOOL finished)
                            { 
                                [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationTransitionNone 
                                                 animations:^{ if (serverIsOn == true) [indicator setAlpha:1.0]; } 
                                                 completion:^(BOOL finished)
                                 { 
                                     isFlashingError = false;
                                 }
                                 ];
                            }
                            ];
                       }
                       ];
                  }
                  ];
             }
             ];
        }
        ];
    }
}

- (void)viewDidUnload
{
    [[GlobalData sharedGlobalData].bluetoothPicker release];
    [[GlobalData sharedGlobalData].bluetoothProgress release];
    [[GlobalData sharedGlobalData].bluetoothIndicator release];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)infoButtonPressed:(id)sender
{
    Infopage * rvController = [[[Infopage alloc] init] autorelease];
    [rvController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [rvController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentModalViewController:rvController animated:true];
}

- (IBAction)contentButtonPressed:(id)sender
{
    Contentpage * rvController = [[[Contentpage alloc] init] autorelease];
    [rvController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [rvController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentModalViewController:rvController animated:true];
}

- (IBAction)setupButtonPressed:(id)sender
{
    Setuppage * rvController = [[[Setuppage alloc] init] autorelease];
    [rvController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [rvController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentModalViewController:rvController animated:true];
}

- (IBAction)switchButtonPressed:(id)sender
{
    if ([switchButton isUserInteractionEnabled] == false)
    {
        return;
    }
    [switchButton setUserInteractionEnabled:false];
    if (serverIsOn == true)
    {
        // Выключаем
        serverIsOn = false;
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationTransitionNone 
                         animations:^{
                             [switcher setFrame:CGRectMake(150, 349, 46, 65)];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
        [UIView animateWithDuration:0.1
                              delay:0.1
                            options:UIViewAnimationTransitionNone 
                         animations:^{
                             [indicator setAlpha:0.0];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
        [UIView animateWithDuration:0.2
                              delay:0.2
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [scroller setAlpha:0.0];
                             [pager setAlpha:0.0];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
        [UIView animateWithDuration:0.2
                              delay:0.4
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [kolpak_zad setFrame:CGRectMake(0, -20, 320, 480)];
                             [kolpak_pered setFrame:CGRectMake(0, -20, 320, 480)];
                         } 
                         completion:^(BOOL finished){
                             @try 
                             {
                                 [[GlobalData sharedGlobalData].httpServer stop];
                                 if([GlobalData sharedGlobalData].FTPServer)
                                 {
                                     [[GlobalData sharedGlobalData].FTPServer stopFtpServer];
                                     [GlobalData sharedGlobalData].FTPServer = nil;
                                 }
                                 [localIpText setText:NSLocalizedString(@"нет соединения", nil)];
                                 [webIpText setText:NSLocalizedString(@"нет соединения", nil)];
                                 [localIpText_ftp setText:NSLocalizedString(@"нет соединения", nil)];
                                 [webIpText_ftp setText:NSLocalizedString(@"нет соединения", nil)];
                             }
                             @catch (NSException * exception)
                             {
                                 //
                             }
                             @finally
                             {
                                 [switchButton setUserInteractionEnabled:true];
                             }
                         }
         ];
        [fileUploadProgress setHidden:true];
    }
    else
    {
        // Включаем
        serverIsOn = true;
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationTransitionNone 
                         animations:^{
                             [switcher setFrame:CGRectMake(125, 349, 46, 65)];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
        [UIView animateWithDuration:0.1
                              delay:0.1
                            options:UIViewAnimationTransitionNone 
                         animations:^{
                             [indicator setAlpha:1.0];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
        [UIView animateWithDuration:0.2
                              delay:0.2
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [kolpak_zad setFrame:CGRectMake(0, -260, 320, 480)];
                             [kolpak_pered setFrame:CGRectMake(0, -260, 320, 480)];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
        [UIView animateWithDuration:0.2
                              delay:0.4
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [scroller setAlpha:1.0];
                             [pager setAlpha:0.8];
                         } 
                         completion:^(BOOL finished){
                             @try 
                             {
                                 [localhostAddresses list];
                                 [[GlobalData sharedGlobalData].httpServer start:nil];
                                 
                                 int port = [[GlobalData sharedGlobalData].connectionPort intValue];
                                 
                                 FtpServer * tempFTPServer = [[FtpServer alloc] initWithPort:port+1 withDir:[VKontakteWorks getRootDir] notifyObject:self];
                                 [GlobalData sharedGlobalData].FTPServer = tempFTPServer;
                                 [tempFTPServer release];
                                 
                                 if ([[GlobalData sharedGlobalData].local_ip_text isEqualToString:@""] == false) 
                                 {
                                     NSString * full_local_ip = [NSString stringWithFormat:@"http://%@:%d", [GlobalData sharedGlobalData].local_ip_text, port];
                                     //[[GlobalData sharedGlobalData].local_ip_text setString:full_local_ip];
                                     [localIpText setText:full_local_ip];
                                     
                                     NSString * full_local_ip_ftp = [NSString stringWithFormat:@"ftp://%@:%d", [GlobalData sharedGlobalData].local_ip_text, port+1];
                                     //[[GlobalData sharedGlobalData].local_ip_text setString:full_local_ip];
                                     [localIpText_ftp setText:full_local_ip_ftp];
                                 }
                                 else
                                 {
                                     [localIpText setText:NSLocalizedString(@"нет соединения", nil)];
                                     [localIpText_ftp setText:NSLocalizedString(@"нет соединения", nil)];
                                 }
                                 
                                 if ([[GlobalData sharedGlobalData].web_ip_text isEqualToString:@""] == false) 
                                 {
                                     NSString * full_web_ip = [NSString stringWithFormat:@"http://%@:%d", [GlobalData sharedGlobalData].web_ip_text, port];
                                     //[[GlobalData sharedGlobalData].web_ip_text setString:full_web_ip];
                                     [webIpText setText:full_web_ip];
                                     
                                     NSString * full_web_ip_ftp = [NSString stringWithFormat:@"ftp://%@:%d", [GlobalData sharedGlobalData].web_ip_text, port+1];
                                     //[[GlobalData sharedGlobalData].web_ip_text setString:full_web_ip];
                                     [webIpText_ftp setText:full_web_ip_ftp];
                                 }
                                 else
                                 {
                                     [webIpText setText:NSLocalizedString(@"нет соединения", nil)];
                                     [webIpText_ftp setText:NSLocalizedString(@"нет соединения", nil)];
                                 }
                             }
                             @catch (NSException * exception)
                             {
                                 //
                             }
                             @finally
                             {
                                 [switchButton setUserInteractionEnabled:true];
                             }
                         }
         ];
    }
}

// -------------- BLUETOOTH: --------------

- (GKSession *)peerPickerController:(GKPeerPickerController *)picker sessionForConnectionType:(GKPeerPickerConnectionType)type 
{
    GKSession * session = [[GKSession alloc] initWithSessionID:@"mix.common.bluetooth.session" displayName:nil sessionMode:GKSessionModePeer];
    return [session autorelease];
}

- (void)peerPickerController:(GKPeerPickerController *)picker didConnectPeer:(NSString *)peerID toSession:(GKSession *)session 
{
    [GlobalData sharedGlobalData].bluetoothSession = session;
    session.delegate = self;
    [picker dismiss];
}

- (void)peerPickerControllerDidCancel:(GKPeerPickerController *)picker
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"bluetoothPickerCanceled" object:nil];
}

- (void)receiveData:(NSData *)data fromPeer:(NSString *)peer inSession:(GKSession *)session context:(void *)context 
{
    @try 
    {
        NSDictionary * bluetoothDictionary = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListMutableContainers format:nil errorDescription:nil];
        NSData * currentData = [bluetoothDictionary objectForKey:@"currentData"];
        NSString * fileName = [bluetoothDictionary objectForKey:@"fileName"];
        NSUInteger currentChunk = [[bluetoothDictionary objectForKey:@"currentChunk"] intValue];
        NSUInteger totalChunks = [[bluetoothDictionary objectForKey:@"totalChunks"] intValue];
        NSUInteger fileSessionNumber = [[bluetoothDictionary objectForKey:@"fileSessionNumber"] intValue];
        
        NSString * saveDirBluetooth = [[VKontakteWorks getRootDir] stringByAppendingPathComponent:NSLocalizedString(@"Bluetooth", nil)];
        if ([[NSFileManager defaultManager] fileExistsAtPath:saveDirBluetooth] == false)
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:saveDirBluetooth withIntermediateDirectories:false attributes:nil error:nil];
        }
        
        NSString * destinationFilePath = [saveDirBluetooth stringByAppendingPathComponent:fileName];
        if ([GlobalData sharedGlobalData].bluetoothFileSessionNumber == -1) 
        {
            if ([[NSFileManager defaultManager] fileExistsAtPath:destinationFilePath] == true)
            {
                NSString * oldFileName = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"(старый)", nil), fileName];
                NSString * destinationOldFilePath = [saveDirBluetooth stringByAppendingPathComponent:oldFileName];
                [[NSFileManager defaultManager] removeItemAtPath:destinationOldFilePath error:nil];
                [[NSFileManager defaultManager] moveItemAtPath:destinationFilePath toPath:destinationOldFilePath error:nil];
            }
            [GlobalData sharedGlobalData].bluetoothFileSessionNumber = fileSessionNumber;
        }
        if (fileSessionNumber != [GlobalData sharedGlobalData].bluetoothFileSessionNumber) 
        {
            return;
        }
        
        if (currentChunk == 1)
        {
            [[GlobalData sharedGlobalData].bluetoothProgress setProgress:0.0];
            [[GlobalData sharedGlobalData].bluetoothIndicator show];
        }
        if (totalChunks != 0) 
        {
            float progress = (float)currentChunk/(float)totalChunks;
            [[GlobalData sharedGlobalData].bluetoothProgress setProgress:progress];
        }
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:destinationFilePath] == false)
        {
            [[NSFileManager defaultManager] createFileAtPath:destinationFilePath contents:currentData attributes:nil];
        }
        else
        {
            NSFileHandle * file = [NSFileHandle fileHandleForUpdatingAtPath:destinationFilePath];
            [file seekToEndOfFile];
            [file writeData:currentData];
        }
        
        NSLog(@"Получаем кусок файла по bluetooth: %d / %d", currentChunk, totalChunks);
        
        if (currentChunk == totalChunks)
        {
            [[GlobalData sharedGlobalData].bluetoothProgress setProgress:1.0];
            [[GlobalData sharedGlobalData].bluetoothIndicator dismissWithClickedButtonIndex:0 animated:true];
            [GlobalData sharedGlobalData].bluetoothFileSessionNumber = -1;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
            
            NSString * mes = [NSString stringWithFormat:NSLocalizedString(@"По каналу Bluetooth принят файл %@", nil), fileName];
            UIAlertView * bluetoothAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Уведомление", nil) message:mes delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
            [bluetoothAlert show];
            [bluetoothAlert release];
        }
    }
    @catch (NSException * exception) 
    {
        //
    }
    @finally
    {
        //
    }
}

- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state 
{
    if (state == GKPeerStateConnected) 
    {
        [[GlobalData sharedGlobalData].bluetoothPeers addObject:peerID];
        [session setDataReceiveHandler:self withContext:nil];
    }
    else if (state == GKPeerStateDisconnected) 
    {
        [[GlobalData sharedGlobalData].bluetoothPeers removeObject:peerID];
    }
}

-(void)didReceiveFileListChanged
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > 0)
    {
        [pager setCurrentPage:1];
    }
    else
    {
        [pager setCurrentPage:0];
    }
}

- (IBAction)howToConnect_localMac:(id)sender
{
    UIAlertView * howToConnect = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"{howToConnect_localMac}", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
    [howToConnect show];
    [howToConnect release];
}

- (IBAction)howToConnect_localWin:(id)sender
{
    UIAlertView * howToConnect = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"{howToConnect_localWin}", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
    [howToConnect show];
    [howToConnect release];
}

- (IBAction)howToConnect_webMac:(id)sender
{
    UIAlertView * howToConnect = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"{howToConnect_webMac}", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
    [howToConnect show];
    [howToConnect release];
}

- (IBAction)howToConnect_webWin:(id)sender
{
    UIAlertView * howToConnect = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"{howToConnect_webWin}", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
    [howToConnect show];
    [howToConnect release];
}

- (void)switchServerOFF
{
    if (serverIsOn == true)
    {
        [self performSelector:@selector(switchButtonPressed:) withObject:nil afterDelay:1.0];
    }
}

@end