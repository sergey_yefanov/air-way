#import "GlobalImport.h"
#import "HTTPServer.h"
#import "MyHTTPConnection.h"
#import "FtpServer.h"

@class HTTPServer;

@interface GlobalData : NSObject 
{
    NSMutableString         * local_ip_text;
    NSMutableString         * web_ip_text;
    NSMutableString         * authPassword;
    HTTPServer              * httpServer;
    NSMutableArray          * contentLibraryArray;
    float                   wifiProgress;
    
    GKSession               * bluetoothSession;
    GKPeerPickerController  * bluetoothPicker;
    NSMutableArray          * bluetoothPeers;
    int                     bluetoothFileSessionNumber;
    UIAlertView             * bluetoothIndicator;
    UIProgressView          * bluetoothProgress;
    
    FtpServer               * FTPServer;
    NSMutableString         * connectionPort;
}

@property (nonatomic, retain) NSMutableString * local_ip_text;
@property (nonatomic, retain) NSMutableString * web_ip_text;
@property (nonatomic, retain) NSMutableString * authPassword;
@property (nonatomic, retain) HTTPServer * httpServer;
@property (nonatomic, retain) NSMutableArray * contentLibraryArray;
@property (nonatomic) float wifiProgress;

@property (nonatomic, retain) GKPeerPickerController * bluetoothPicker;
@property (nonatomic, retain) NSMutableArray * bluetoothPeers;
@property (nonatomic, retain) GKSession * bluetoothSession;
@property (nonatomic) int bluetoothFileSessionNumber;
@property (nonatomic, retain) UIAlertView * bluetoothIndicator;
@property (nonatomic, retain) UIProgressView * bluetoothProgress;

@property (nonatomic, retain) FtpServer * FTPServer;
@property (nonatomic, retain) NSMutableString * connectionPort;

+ (GlobalData *)sharedGlobalData;

@end