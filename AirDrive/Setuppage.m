#import "Setuppage.h"

@implementation Setuppage

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [authField setText:[GlobalData sharedGlobalData].authPassword];
    [portField setText:[GlobalData sharedGlobalData].connectionPort];
    
    [authHeader setText:NSLocalizedString(@"Защита соединения:", nil)];
    [authField setPlaceholder:NSLocalizedString(@"без пароля", nil)];
    [portHeader setText:NSLocalizedString(@"Порт для соединения (0-65535):", nil)];
    [portField setPlaceholder:NSLocalizedString(@"стандартный порт 8080", nil)];
    [btHeader setText:NSLocalizedString(@"Bluetooth:", nil)];
    [btDescription setText:NSLocalizedString(@"описание блютуса", nil)];
    
    if ([GlobalData sharedGlobalData].bluetoothSession)
    {
        [btSwitcher setFrame:CGRectMake(125, 249, 46, 65)];
    }
    else
    {
        [btSwitcher setFrame:CGRectMake(150, 249, 46, 65)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bluetoothPickerCanceled)
                                                 name:@"bluetoothPickerCanceled"
                                               object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)cancelButtonPressed:(id)sender
{
    [self dismissModalViewControllerAnimated:true];
}

- (IBAction)saveButtonPressed:(id)sender
{
    NSString * old_port = [[GlobalData sharedGlobalData].connectionPort copy];
    
    [[GlobalData sharedGlobalData].authPassword setString:[authField text]];
    [[GlobalData sharedGlobalData].connectionPort setString:[portField text]];
    int port;
    @try 
    {
        port = [[GlobalData sharedGlobalData].connectionPort intValue];
        if (port < 1024)
        {
            port = 8080;
        }
        if (port > 32768)
        {
            port = 8080;
        }
        [[GlobalData sharedGlobalData].connectionPort setString:[NSString stringWithFormat:@"%d", port]];
        [[GlobalData sharedGlobalData].httpServer setPort:[GlobalData sharedGlobalData].connectionPort.intValue];
        /*if ([GlobalData sharedGlobalData].FTPServer)
        {
            [[GlobalData sharedGlobalData].FTPServer setPortNumber:[GlobalData sharedGlobalData].connectionPort.intValue+1];
        }*/
    }
    @catch (NSException * exception) 
    {
        [[GlobalData sharedGlobalData].connectionPort setString:@"8080"];
        [[GlobalData sharedGlobalData].httpServer setPort:[GlobalData sharedGlobalData].connectionPort.intValue];
        /*if ([GlobalData sharedGlobalData].FTPServer)
        {
            [[GlobalData sharedGlobalData].FTPServer setPortNumber:[GlobalData sharedGlobalData].connectionPort.intValue+1];
        }*/
    }
    @finally 
    {
        //
    }
    
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    [settings setValue:[GlobalData sharedGlobalData].authPassword forKey:@"authPassword"];
    [settings setValue:[GlobalData sharedGlobalData].connectionPort forKey:@"connectionPort"];
    if ([settings synchronize] == true) { NSLog(@"Настройки сохранены"); } else { NSLog(@"Глюк с сохранением настроек"); }
    
    [self dismissModalViewControllerAnimated:true];
    
    NSString * new_port = [[GlobalData sharedGlobalData].connectionPort copy];

    if ([old_port isEqualToString:new_port] == false)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"switchServerOFF" object:nil];
    }
}

- (IBAction)textFieldReturn:(id)sender
{
	[sender resignFirstResponder];
}

- (IBAction)btSwitchButtonPressed:(id)sender
{
    [btSwitchButton setUserInteractionEnabled:false];
    if ([GlobalData sharedGlobalData].bluetoothSession)
    {
        // Выключаем блютус
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationTransitionNone 
                         animations:^{
                             [btSwitcher setFrame:CGRectMake(150, 249, 46, 65)];
                         } 
                         completion:^(BOOL finished){
                             [[GlobalData sharedGlobalData].bluetoothSession disconnectFromAllPeers];
                             [GlobalData sharedGlobalData].bluetoothSession = nil;
                             [[GlobalData sharedGlobalData].bluetoothPeers removeAllObjects];
                             [btSwitchButton setUserInteractionEnabled:true];
                         }
         ];
    }
    else
    {
        // Включаем блютус
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationTransitionNone 
                         animations:^{
                             [btSwitcher setFrame:CGRectMake(125, 249, 46, 65)];
                         } 
                         completion:^(BOOL finished){
                             [[GlobalData sharedGlobalData].bluetoothPicker show];
                             [btSwitchButton setUserInteractionEnabled:true];
                         }
         ];
    }
}

- (void)bluetoothPickerCanceled
{
    [btSwitchButton setUserInteractionEnabled:false];
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationTransitionNone 
                     animations:^{
                         [btSwitcher setFrame:CGRectMake(150, 249, 46, 65)];
                     } 
                     completion:^(BOOL finished){
                         [btSwitchButton setUserInteractionEnabled:true];
                     }
     ];
}

@end