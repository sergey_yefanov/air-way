#import "Contentpage.h"
#import "LibraryCell.h"
#import "SSZipArchive.h"

@implementation Contentpage

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)canBecomeFirstResponder
{
    return true;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self becomeFirstResponder];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event 
{
    @try 
    {
        if (event.type == UIEventSubtypeMotionShake) 
        {
            NSLog(@"Трясём девайс 1");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
        }
    }
    @catch (NSException * exception) 
    {
        //
    }
    @finally
    {
        //
    }
}

- (void)updateSpaceIndicators
{
    if (mayUpdateProgress == false) return;
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    @try 
    {
        mayUpdateProgress = false;
        NSString * rootDir = [VKontakteWorks getRootDir];
        
        NSDirectoryEnumerator * dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:rootDir];
        float totalSpace = 0;
        NSString * fileOrDir;
        while (fileOrDir = [dirEnum nextObject])
        {
            NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:fileOrDir] error:nil];
            if (fileAttr != nil) totalSpace = totalSpace + [[fileAttr objectForKey:NSFileSize] floatValue]; 
        }
        totalSpace = totalSpace / 1048576;
        
        NSDictionary * dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:rootDir error:nil];
        NSNumber * freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        float totalFreeSpace = 0;
        totalFreeSpace = [freeFileSystemSizeInBytes floatValue];
        totalFreeSpace = totalFreeSpace / 1048576;
        
        float progress = 0;
        if ((totalSpace + totalFreeSpace) > 0) 
        {
            progress = totalSpace / (totalSpace + totalFreeSpace);
        }
        
        [contentSpaceProgress setProgress:progress];
        
        contentSpaceLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Занято: %.0f Мб | Свободно: %.0f Мб", nil), totalSpace, totalFreeSpace];
    }
    @catch (NSException * exception)
    {
        //
    }
    @finally
    {
        mayUpdateProgress = true;
        [pool release];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shared_filePath = [[NSMutableString alloc] init]; 
    shared_fileName = [[NSMutableString alloc] init];
    shared_filePath_Rename = [[NSMutableString alloc] init];
    newNameField = [[UITextField alloc] init];
    pathToDir = [[NSMutableArray alloc] init];
    dirLevel = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(needUpdateStorage)
                                                 name:@"needUpdateStorage"
                                               object:nil];
    
    mayUpdateProgress = true;
    
    [noFiles setText:NSLocalizedString(@"Нет файлов", nil)];
    [contentSpaceLabel setText:NSLocalizedString(@"Занято: неизвестно | Свободно: неизвестно", nil)];
    
    NSString * saveDirImportedPhotos = [[VKontakteWorks getRootDir] stringByAppendingPathComponent:NSLocalizedString(@"Импортированные фото", nil)];
    if ([[NSFileManager defaultManager] fileExistsAtPath:saveDirImportedPhotos] == false)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:saveDirImportedPhotos withIntermediateDirectories:false attributes:nil error:nil];
    }
    
    [importPhotosView.layer setCornerRadius:5.0f];
    [importPhotosButton setTitle:NSLocalizedString(@"Импортировать", nil) forState:UIControlStateNormal];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [pathToDir release];
    [shared_filePath release];
    [shared_fileName release];
    [shared_filePath_Rename release];
    [newNameField release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)closeContentPage:(id)sender
{
    [self dismissModalViewControllerAnimated:true];
}

int finderSortWithLocale(id string1, id string2, void * locale)
{
    static NSStringCompareOptions comparisonOptions =
    NSCaseInsensitiveSearch | NSNumericSearch |
    NSWidthInsensitiveSearch | NSForcedOrderingSearch;
    
    NSRange string1Range = NSMakeRange(0, [string1 length]);
    
    return [string1 compare:string2
                    options:comparisonOptions
                      range:string1Range
                     locale:(NSLocale *)locale];
}

- (void)reloadLibrary 
{
    [[GlobalData sharedGlobalData].contentLibraryArray removeAllObjects];
    
    NSString * rootDir = [VKontakteWorks getRootDir];
    for (int i = 0; i < dirLevel; i++) 
    {
        rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
    }
    
    NSArray * tempArray1 = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:rootDir error:nil];
    NSArray * tempArray = [tempArray1 sortedArrayUsingFunction:finderSortWithLocale context:[NSLocale currentLocale]];
    for (int i = 0; i < [tempArray count]; i++)
    {
        NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:[tempArray objectAtIndex:i]] error:nil];
        if (fileAttr != nil)
        {
            if ([fileAttr objectForKey:NSFileType] != NSFileTypeDirectory) 
            {
                [[GlobalData sharedGlobalData].contentLibraryArray addObject:[tempArray objectAtIndex:i]];
            }
        }
    }
    
    for (int i = [tempArray count]-1; i >= 0; i--)
    {
        NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:[tempArray objectAtIndex:i]] error:nil];
        if (fileAttr != nil)
        {
            if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory) 
            {
                [[GlobalData sharedGlobalData].contentLibraryArray insertObject:[tempArray objectAtIndex:i] atIndex:0];
            }
        }
    }

    if ([pathToDir count] == 0)
    {
        currentFolder.text = @"";
        [UIView animateWithDuration:0.2 animations:^{
            [backButton setAlpha:0.0];
        }];
    }
    else
    {
        currentFolder.text = [NSString stringWithFormat:@"%@", [pathToDir lastObject]];
        [UIView animateWithDuration:0.2 animations:^{
            if (contentTable.editing == false) [backButton setAlpha:1.0];
        }];
    }
    
    if ([[GlobalData sharedGlobalData].contentLibraryArray count] > 0) 
    {
        [noFiles setHidden:true];
    }
    else
    {
        [noFiles setHidden:false];
    }

    [contentTable reloadData];
    if ([contentTable numberOfRowsInSection:0] > 0)
    {
        [contentTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:false];
    }
    
    NSString * saveDirImportedPhotos = [[VKontakteWorks getRootDir] stringByAppendingPathComponent:NSLocalizedString(@"Импортированные фото", nil)];
    if ([rootDir isEqualToString:saveDirImportedPhotos] == true) 
    {
        [importPhotosView setHidden:false];
    }
    else
    {
        [importPhotosView setHidden:true];
    }
}

- (void)needUpdateStorage
{
    [self reloadLibrary];
    [self performSelectorInBackground:@selector(updateSpaceIndicators) withObject:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setDoNotBackup" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self needUpdateStorage];
}

- (IBAction)backInTree:(id)sender
{
    if ([pathToDir count] > 0) 
    {
        [pathToDir removeLastObject];
        dirLevel--;
        [contentTable setCenter:CGPointMake(contentTable.center.x-50, contentTable.center.y)];
        [contentTable setAlpha:0.0];
        [UIView animateWithDuration:0.3 animations:^{
            [contentTable setCenter:CGPointMake(contentTable.center.x+50, contentTable.center.y)];
            [contentTable setAlpha:1.0];
        }];
        [self reloadLibrary];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString * CellIdentifier = @"LibraryCell";
    
    LibraryCell * cell = (LibraryCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        UIViewController * c = [[UIViewController alloc] initWithNibName:CellIdentifier bundle:nil];
        cell = (LibraryCell *)c.view;
        [c release];
    }
    
    if ([[GlobalData sharedGlobalData].contentLibraryArray count] <= [indexPath row]) return cell;

    NSString * tempFileName = [[GlobalData sharedGlobalData].contentLibraryArray objectAtIndex:[indexPath row]];
    
    NSString * rootDir = [VKontakteWorks getRootDir];
    for (int i = 0; i < dirLevel; i++) 
    {
        rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
    }
    
    NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:tempFileName] error:nil];
    if (fileAttr != nil)
    {
        if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory) 
        {
            cell.elementName.text = [tempFileName lastPathComponent];
            cell.elementDetails.text = NSLocalizedString(@"папка с файлами", nil);
            cell.elementImage.image = [UIImage imageNamed:@"folder.png"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.tag = 1;
        }
        else
        {
            cell.elementName.text = [tempFileName lastPathComponent];
            cell.elementImage.image = [UIImage imageNamed:@"file.png"];
            
            NSString * filePath1 = [rootDir stringByAppendingPathComponent:tempFileName];
            NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath1 error:nil];
            float size = 0;
            if (fileAttr != nil) size = [[fileAttr objectForKey:NSFileSize] floatValue]; 
            size = size / 1048576 * 1024;
            cell.elementDetails.text = [NSString stringWithFormat:NSLocalizedString(@"%.1f Кб", nil), size];
        }
    }
    
    NSString * manulFileName = [[[tempFileName lastPathComponent] stringByDeletingPathExtension] lowercaseString];
    NSString * manulWord = NSLocalizedString(@"манул", nil);
    if ([manulFileName rangeOfString:manulWord].length > 0)
    {
        [cell.elementImage setFrame:CGRectMake(6, 6, 48, 48)];
        cell.elementImage.image = [UIImage imageNamed:@"manul.png"];
    }
    
    [[cell.editButton titleLabel] setText:[rootDir stringByAppendingPathComponent:tempFileName]];
    [cell.editButton addTarget:self action:@selector(beginEditingFile:event:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSString * rootDir = [VKontakteWorks getRootDir];
    for (int i = 0; i < dirLevel; i++)
    {
        rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
    }
    NSString * tempFileName = [[GlobalData sharedGlobalData].contentLibraryArray objectAtIndex:[indexPath row]];
    NSString * tempFilePath = [rootDir stringByAppendingPathComponent:tempFileName];
    [shared_filePath setString:tempFilePath];
    [shared_fileName setString:tempFileName];
    
    NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:tempFileName] error:nil];
    if (fileAttr != nil)
    {
        if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory) 
        {
            [pathToDir addObjectsFromArray:[tempFileName pathComponents]];
            dirLevel++;
            [contentTable setCenter:CGPointMake(contentTable.center.x+50, contentTable.center.y)];
            [contentTable setAlpha:0.0];
            [UIView animateWithDuration:0.3 animations:^{
                [contentTable setCenter:CGPointMake(contentTable.center.x-50, contentTable.center.y)];
                [contentTable setAlpha:1.0];
            }];
            [self reloadLibrary];
        }
        else
        {
            [contentTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:true];
            
            NSArray * ArchiveExt = [NSArray arrayWithObjects:@"zip", @"rar", nil];
            NSArray * ImagesExt = [NSArray arrayWithObjects:@"png", @"tif", @"tiff", @"jpeg", @"jpg", @"gif", @"bmp", @"bmpf", @"ico", @"cur", @"xbm", nil];
            NSString * ext = [[shared_fileName pathExtension] lowercaseString];
            
            if ([ArchiveExt containsObject:ext] == true)
            {
                UIActionSheet * actions = [[UIActionSheet alloc] initWithTitle:shared_fileName delegate:self cancelButtonTitle:NSLocalizedString(@"Отмена", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Разархивировать", nil), NSLocalizedString(@"Открыть с помощью...", nil), NSLocalizedString(@"Отправить по e-mail", nil), NSLocalizedString(@"Отправить по bluetooth", nil), nil];
                [actions setTag:1];
                [actions showInView:self.view];
                [actions release];
            }
            else if ([ImagesExt containsObject:ext] == true)
            {
                UIActionSheet * actions = [[UIActionSheet alloc] initWithTitle:shared_fileName delegate:self cancelButtonTitle:NSLocalizedString(@"Отмена", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Открыть", nil), NSLocalizedString(@"Открыть с помощью...", nil), NSLocalizedString(@"Отправить по e-mail", nil), NSLocalizedString(@"Отправить по bluetooth", nil), NSLocalizedString(@"Сохранить в Фото", nil), nil];
                [actions setTag:1];
                [actions showInView:self.view];
                [actions release];
            }
            else
            {
                UIActionSheet * actions = [[UIActionSheet alloc] initWithTitle:shared_fileName delegate:self cancelButtonTitle:NSLocalizedString(@"Отмена", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Открыть", nil), NSLocalizedString(@"Открыть с помощью...", nil), NSLocalizedString(@"Отправить по e-mail", nil), NSLocalizedString(@"Отправить по bluetooth", nil), nil];
                [actions setTag:1];
                [actions showInView:self.view];
                [actions release];
            }
        }
    }
}

/*- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        NSString * rootDir = [VKontakteWorks getRootDir];
        for (int i = 0; i < dirLevel; i++)
        {
            rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
        }
        NSString * filePath = [rootDir stringByAppendingPathComponent:[[GlobalData sharedGlobalData].contentLibraryArray objectAtIndex:indexPath.row]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] == true)
        {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    NSString * rootDir = [VKontakteWorks getRootDir];
    for (int i = 0; i < dirLevel; i++)
    {
        rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
    }
    NSString * fileName = [[GlobalData sharedGlobalData].contentLibraryArray objectAtIndex:[indexPath row]];
    
    NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:fileName] error:nil];
    if (fileAttr != nil)
    {
        if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory) 
        {
            return NSLocalizedString(@"Удалить папку", nil);
        }
        else
        {
            return NSLocalizedString(@"Удалить файл", nil);
        }
    }
    else
    {
        return NSLocalizedString(@"Удалить файл", nil);
    }
}*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [[GlobalData sharedGlobalData].contentLibraryArray count];      
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error 
{   
    switch (result)    
    {        
        case MFMailComposeResultCancelled:            
            NSLog(@"Result: canceled");            
            break;        
        case MFMailComposeResultSaved:            
            NSLog(@"Result: saved");            
            break;        
        case MFMailComposeResultSent:            
            NSLog(@"Result: sent");            
            break;        
        case MFMailComposeResultFailed:            
            NSLog(@"Result: failed");            
            break;        
        default:            
            NSLog(@"Result: not sent");            
            break;    
    }    
    [self dismissModalViewControllerAnimated:true];
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
    //
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application
{
    //
}

- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
    //
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (void)sendFileByBluetooth:(NSString *)filePath
{
    @try 
    {
        if ([GlobalData sharedGlobalData].bluetoothSession)
        {
            NSString * fileName = [filePath lastPathComponent];
            NSData * bluetoothData = [NSData dataWithContentsOfFile:filePath];
            NSUInteger fiftyK = 51200;
            NSUInteger addingNum = 0;
            
            if ((bluetoothData.length % fiftyK) == 0) { addingNum = 0; } else { addingNum = 1; }
            NSUInteger totalChunks = (NSUInteger)(bluetoothData.length / fiftyK) + addingNum;
            
            NSData * currentData;
            NSUInteger currentChunk = 0;
            srandom(time(nil));
            NSUInteger fileSessionNumber = arc4random() % 1000000;
            
            for (NSUInteger i = 0; i < totalChunks-1; i++)
            {
                currentChunk++;
                NSRange range = {i*fiftyK, fiftyK};
                currentData = [bluetoothData subdataWithRange:range];
                NSArray * btObjectsArray = [NSArray arrayWithObjects:currentData, fileName, [NSString stringWithFormat:@"%d", currentChunk], [NSString stringWithFormat:@"%d", totalChunks], [NSString stringWithFormat:@"%d", fileSessionNumber], nil];
                NSArray * btKeysArray = [NSArray arrayWithObjects:@"currentData", @"fileName", @"currentChunk", @"totalChunks", @"fileSessionNumber", nil];
                NSDictionary * bluetoothDictionary = [[NSDictionary alloc] initWithObjects:btObjectsArray forKeys:btKeysArray];
                NSData * encodedBluetoothArray = [NSPropertyListSerialization dataFromPropertyList:bluetoothDictionary format:NSPropertyListBinaryFormat_v1_0 errorDescription:nil];
                NSLog(@"Отправляем кусок файла по bluetooth: %d / %d", currentChunk, totalChunks);
                [[GlobalData sharedGlobalData].bluetoothSession sendDataToAllPeers:encodedBluetoothArray withDataMode:GKSendDataReliable error:nil];
                [bluetoothDictionary release];
            }
            NSUInteger remainder = (bluetoothData.length % fiftyK);
            if (addingNum == 1)
            {
                currentChunk++;
                NSRange range = {bluetoothData.length - remainder, remainder};
                currentData = [bluetoothData subdataWithRange:range];
                NSArray * btObjectsArray = [NSArray arrayWithObjects:currentData, fileName, [NSString stringWithFormat:@"%d", currentChunk], [NSString stringWithFormat:@"%d", totalChunks], [NSString stringWithFormat:@"%d", fileSessionNumber], nil];
                NSArray * btKeysArray = [NSArray arrayWithObjects:@"currentData", @"fileName", @"currentChunk", @"totalChunks", @"fileSessionNumber", nil];
                NSDictionary * bluetoothDictionary = [[NSDictionary alloc] initWithObjects:btObjectsArray forKeys:btKeysArray];
                NSData * encodedBluetoothArray = [NSPropertyListSerialization dataFromPropertyList:bluetoothDictionary format:NSPropertyListBinaryFormat_v1_0 errorDescription:nil];
                NSLog(@"Отправляем кусок файла по bluetooth: %d / %d", currentChunk, totalChunks);
                [[GlobalData sharedGlobalData].bluetoothSession sendDataToAllPeers:encodedBluetoothArray withDataMode:GKSendDataReliable error:nil];
                [bluetoothDictionary release];
            }
        }
        else
        {
            UIAlertView * bluetoothAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil) message:NSLocalizedString(@"Сначала установите соединение с другими устройствами в разделе настроек", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
            [bluetoothAlert show];
            [bluetoothAlert release];
        }
    }
    @catch (NSException * exception) 
    {
        //
    }
    @finally
    {
        //
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([actionSheet tag] == 1)
    {
        if (buttonIndex == 0)
        {
            NSArray * MediaExt = [NSArray arrayWithObjects:@"mov", @"mp4", @"mpv", @"3gp", @"mp3", @"aac", @"wav", @"caf", @"m4v", @"m4p", @"m4a", @"aiff", nil];
            NSString * ext = [[shared_filePath pathExtension] lowercaseString];
            
            if ([ext isEqualToString:@"zip"] == true)
            {
                @try
                {
                    NSString * unzip_path = [shared_filePath stringByDeletingLastPathComponent];
                    [SSZipArchive unzipFileAtPath:shared_filePath toDestination:unzip_path];
                }
                @catch (NSException * exception) 
                {
                    UIAlertView * zipAlert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:NSLocalizedString(@"При открытии архива произошла ошибка", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil];
                    [zipAlert show];
                    [zipAlert release];
                }
                @finally 
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
                }
            }
            else if ([ext isEqualToString:@"rar"] == true)
            {
                @try
                {
                    Unrar4iOS * unrar = [[Unrar4iOS alloc] init];
                    BOOL ok = [unrar unrarOpenFile:shared_filePath];
                    if (ok) 
                    {
                        NSString * currentRarFolder = [shared_filePath stringByDeletingLastPathComponent];
                        NSArray * rarFiles = [unrar unrarListFiles];
                        for (NSString * rarName in rarFiles) 
                        {
                            NSString * rarExt = [rarName pathExtension];
                            if ([rarExt isEqualToString:@""] == true) 
                            {
                                if ([[NSFileManager defaultManager] fileExistsAtPath:[currentRarFolder stringByAppendingPathComponent:rarName]] == false)
                                {
                                    NSLog(@"Создаю папку: %@", rarName);
                                    [[NSFileManager defaultManager] createDirectoryAtPath:[currentRarFolder stringByAppendingPathComponent:rarName] withIntermediateDirectories:true attributes:nil error:nil];
                                }
                            }
                        }
                        for (NSString * rarName in rarFiles) 
                        {
                            NSString * rarExt = [rarName pathExtension];
                            if ([rarExt isEqualToString:@""] == false) 
                            {
                                NSLog(@"Распаковываю файл: %@", rarName);
                                NSData * data = [unrar extractStream:rarName];
                                [[NSFileManager defaultManager] createFileAtPath:[currentRarFolder stringByAppendingPathComponent:rarName] contents:data attributes:nil];
                            }
                        }
                    }
                    [unrar unrarCloseFile];
                    [unrar release];
                }
                @catch (NSException * exception) 
                {
                    UIAlertView * zipAlert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:NSLocalizedString(@"При открытии архива произошла ошибка", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil];
                    [zipAlert show];
                    [zipAlert release];
                }
                @finally
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
                }
            }
            else if ([MediaExt containsObject:ext] == true) 
            {
                @try 
                {
                    AVAudioSession * audioSession = [AVAudioSession sharedInstance];
                    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
                    [audioSession setActive:true error:nil];
                    
                    MPMoviePlayerViewController * mediaController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:shared_filePath]];
                    if ([mediaController.moviePlayer respondsToSelector:@selector(setAllowsAirPlay:)]) [mediaController.moviePlayer setAllowsAirPlay:true];
                    [mediaController.moviePlayer setRepeatMode:MPMovieRepeatModeOne];
                    [self presentModalViewController:mediaController animated:false];
                    [mediaController.moviePlayer setUseApplicationAudioSession:true];
                    [mediaController.moviePlayer prepareToPlay];
                    [mediaController.moviePlayer play];
                }
                @catch (NSException * exception) 
                {
                    //
                }
                @finally 
                {
                    //
                }
            }
            else
            {
                @try 
                {
                    UIDocumentInteractionController * fileController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:shared_filePath]];
                    [fileController setDelegate:self];
                    [fileController retain];
                    if ([fileController presentPreviewAnimated:true] == false)
                    {
                        UIAlertView * infoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil) message:NSLocalizedString(@"Невозможно открыть данный тип файлов", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
                        [infoAlert show];
                        [infoAlert release];
                        [fileController release];
                    }
                }
                @catch (NSException * exception) 
                {
                    //
                }
                @finally 
                {
                    //
                }
            }
        }
        
        if (buttonIndex == 1)
        {
            @try 
            {
                UIDocumentInteractionController * fileController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:shared_filePath]];
                [fileController setDelegate:self];
                [fileController retain];
                if ([fileController presentOpenInMenuFromRect:CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 0, 0) inView:self.view animated:true] == false)
                {
                    UIAlertView * infoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil) message:NSLocalizedString(@"Не найдено ни одного подходящего приложения для экспорта данного типа файлов", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
                    [infoAlert show];
                    [infoAlert release];
                    [fileController release];
                }
            }
            @catch (NSException * exception) 
            {
                //
            }
            @finally 
            {
                //
            }
        }
        
        if (buttonIndex == 2) 
        {
            @try
            {
                if ([MFMailComposeViewController canSendMail] == true) 
                {
                    MFMailComposeViewController * mail = [[MFMailComposeViewController alloc] init];    
                    mail.mailComposeDelegate = self;    
                    [mail setSubject:shared_fileName];
                    NSURL * url = [[NSURL alloc] initFileURLWithPath:shared_filePath];
                    NSData * file = [[NSData alloc] initWithContentsOfURL:url];
                    [mail addAttachmentData:file mimeType:@"application/octet-stream" fileName:shared_fileName];      
                    [mail setMessageBody:NSLocalizedString(@"Файл отправлен из приложения AirDrive", nil) isHTML:false];    
                    [self presentModalViewController:mail animated:true];    
                    [mail release];
                    [file release];
                    [url release];
                }
                else
                {
                    UIAlertView * mailAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil) message:NSLocalizedString(@"Не настроена учётная запись для отправки почты", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
                    [mailAlert show];
                    [mailAlert release];
                }
            }
            @catch (NSException * exception) 
            {
                //
            }
            @finally 
            {
                //
            }
        }
        
        if (buttonIndex == 3) 
        {
            @try 
            {
                NSLog(@"Bluetooth: %@", shared_filePath);
                [self sendFileByBluetooth:shared_filePath];
            }
            @catch (NSException * exception) 
            {
                //
            }
            @finally 
            {
                //
            }
        }
        
        if (buttonIndex == 4) 
        {
            NSArray * ImagesExt = [NSArray arrayWithObjects:@"png", @"tif", @"tiff", @"jpeg", @"jpg", @"gif", @"bmp", @"bmpf", @"ico", @"cur", @"xbm", nil];
            NSString * ext = [[shared_filePath pathExtension] lowercaseString];
            if ([ImagesExt containsObject:ext] == true)
            {
                NSLog(@"Сохранить в Фото");
                @try 
                {
                    UIImage * theImage = [UIImage imageWithContentsOfFile:shared_filePath];
                    UIImageWriteToSavedPhotosAlbum(theImage, self, nil, nil);
                }
                @catch (NSException * exception) 
                {
                    //
                }
                @finally
                {
                    //
                }
            }
        }
    }
    
    if ([actionSheet tag] == 2)
    {
        //
    }
    
    if ([actionSheet tag] == 3)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Переименовать: %@", shared_filePath);
            UIAlertView * newName = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Новое имя:", nil) message:@" \n " delegate:self cancelButtonTitle:NSLocalizedString(@"Отмена", nil) otherButtonTitles:NSLocalizedString(@"Принять", nil), nil];
            
            [newNameField setFrame:CGRectMake(11.0, 52.0, 262.0, 31.0)];
            
            [newNameField setBorderStyle:UITextBorderStyleRoundedRect];
            float version = [[[UIDevice currentDevice] systemVersion] floatValue];
            if (version >= 5.0)
            {
                [newNameField setBackgroundColor:[UIColor whiteColor]];
            }
            else
            {
                [newNameField setBackgroundColor:[UIColor clearColor]];
            }
            [newNameField setClearButtonMode:UITextFieldViewModeAlways];
            [newNameField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [newNameField setReturnKeyType:UIReturnKeyDone];
            [newNameField setAutocorrectionType:UITextAutocorrectionTypeNo];
            [newNameField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            [newNameField setPlaceholder:nil];
            [newNameField setText:[shared_filePath lastPathComponent]];
            
            [newName addSubview:newNameField];
            [newName setTag:1];
            [newName show];
            [newName release];
        }
        if (buttonIndex == 1)
        {
            NSLog(@"Копировать: %@", shared_filePath);
            if ([[NSFileManager defaultManager] fileExistsAtPath:shared_filePath] == true)
            {
                NSString * fileExt;
                NSString * baseCopyName;
                NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:shared_filePath error:nil];
                if (fileAttr != nil)
                {
                    if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory) 
                    {
                        baseCopyName = [NSString stringWithFormat:@"%@ - %@", shared_filePath, NSLocalizedString(@"копия", nil)];
                        NSMutableString * copy_to = [[NSMutableString alloc] initWithString:baseCopyName];
                        int j = 0;
                        while ([[NSFileManager defaultManager] fileExistsAtPath:copy_to] == true)
                        {
                            j++;
                            [copy_to setString:[NSString stringWithFormat:@"%@ %d", baseCopyName, j]];
                        }
                        [[NSFileManager defaultManager] copyItemAtPath:shared_filePath toPath:copy_to error:nil];
                        [copy_to release];
                    }
                    else
                    {
                        fileExt = [shared_filePath pathExtension];
                        baseCopyName = [NSString stringWithFormat:@"%@ - %@", [shared_filePath stringByDeletingPathExtension], NSLocalizedString(@"копия", nil)];
                        NSMutableString * copy_to = [[NSMutableString alloc] initWithString:baseCopyName];
                        int j = 0;
                        while ([[NSFileManager defaultManager] fileExistsAtPath:[copy_to stringByAppendingPathExtension:fileExt]] == true)
                        {
                            j++;
                            [copy_to setString:[NSString stringWithFormat:@"%@ %d", baseCopyName, j]];
                        }
                        [copy_to setString:[copy_to stringByAppendingPathExtension:fileExt]];
                        [[NSFileManager defaultManager] copyItemAtPath:shared_filePath toPath:copy_to error:nil];
                        [copy_to release];
                    }
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
        }
        if (buttonIndex == 2)
        {
            NSLog(@"Переместить: %@", shared_filePath);
            if ([[NSFileManager defaultManager] fileExistsAtPath:shared_filePath] == true)
            {
                UIActionSheet * actions;
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    actions = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"На уровень вверх", nil), nil];
                }
                else
                {
                    actions = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Отмена", nil), NSLocalizedString(@"На уровень вверх", nil), nil];
                }
                
                
                NSString * rootDir = [shared_filePath stringByDeletingLastPathComponent];
                NSMutableArray * folders = [[NSMutableArray alloc] init];
                NSArray * tempArray1 = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:rootDir error:nil];
                NSArray * tempArray = [tempArray1 sortedArrayUsingFunction:finderSortWithLocale context:[NSLocale currentLocale]];
                for (int i = 0; i < [tempArray count]; i++)
                {
                    NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:[tempArray objectAtIndex:i]] error:nil];
                    if (fileAttr != nil)
                    {
                        if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory) 
                        {
                            [folders addObject:[tempArray objectAtIndex:i]];
                        }
                    }
                }
                
                for (NSString * folder in folders) [actions addButtonWithTitle:folder];
                [folders release];
                [actions setTag:4];
                [actions showFromRect:CGRectMake(400, 0, 624, 748) inView:self.view animated:true];
                [actions release];
            }
        }
        if (buttonIndex == 3)
        {
            NSLog(@"Удалить: %@", shared_filePath);
            if ([[NSFileManager defaultManager] fileExistsAtPath:shared_filePath] == true)
            {
                [[NSFileManager defaultManager] removeItemAtPath:shared_filePath error:nil];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
        }
    }
    
    if ([actionSheet tag] == 4)
    {
        if (buttonIndex > 10000000) buttonIndex = -1;
        NSString * rootDir = [shared_filePath stringByDeletingLastPathComponent];
        NSMutableArray * folders = [[NSMutableArray alloc] init];
        NSArray * tempArray1 = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:rootDir error:nil];
        NSArray * tempArray = [tempArray1 sortedArrayUsingFunction:finderSortWithLocale context:[NSLocale currentLocale]];
        for (int i = 0; i < [tempArray count]; i++)
        {
            NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:[rootDir stringByAppendingPathComponent:[tempArray objectAtIndex:i]] error:nil];
            if (fileAttr != nil)
            {
                if ([fileAttr objectForKey:NSFileType] == NSFileTypeDirectory)
                {
                    [folders addObject:[tempArray objectAtIndex:i]];
                }
            }
        }
        NSString * selectedFolder;
        NSString * movingTo;
        int action = 0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            if (buttonIndex == 0)
            {
                // На уровень вверх
                action = 1;
                if ([rootDir isEqualToString:[VKontakteWorks getRootDir]] == true) 
                {
                    selectedFolder = rootDir;
                    NSLog(@"top level");
                }
                else
                {
                    selectedFolder = [rootDir stringByDeletingLastPathComponent];
                }
                movingTo = [selectedFolder stringByAppendingPathComponent:[shared_filePath lastPathComponent]];
            }
            else if (buttonIndex > 0)
            {
                // В папку такую-то
                action = 2;
                selectedFolder = [rootDir stringByAppendingPathComponent:[folders objectAtIndex:buttonIndex-1]];
                movingTo = [selectedFolder stringByAppendingPathComponent:[shared_filePath lastPathComponent]];
            }
        }
        else
        {
            if (buttonIndex == 0)
            {
                // Отмена
                action = 0;
            }
            else if (buttonIndex == 1)
            {
                // На уровень вверх
                action = 1;
                if ([rootDir isEqualToString:[VKontakteWorks getRootDir]] == true) 
                {
                    selectedFolder = rootDir;
                    NSLog(@"top level");
                }
                else
                {
                    selectedFolder = [rootDir stringByDeletingLastPathComponent];
                }
                movingTo = [selectedFolder stringByAppendingPathComponent:[shared_filePath lastPathComponent]];
            }
            else if (buttonIndex > 1)
            {
                // В папку такую-то
                action = 2;
                selectedFolder = [rootDir stringByAppendingPathComponent:[folders objectAtIndex:buttonIndex-2]];
                movingTo = [selectedFolder stringByAppendingPathComponent:[shared_filePath lastPathComponent]];
            }
        }
        if (action == 0) 
        {
            // Отмена
        }
        else if (action == 1)
        {
            // На уровень вверх
            [[NSFileManager defaultManager] moveItemAtPath:shared_filePath toPath:movingTo error:nil];
        }
        else if (action == 2)
        {
            // В папку такую-то
            [[NSFileManager defaultManager] moveItemAtPath:shared_filePath toPath:movingTo error:nil];
        }
        [folders release];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
    }
}

- (IBAction)enterEditMode:(id)sender
{
    BOOL editing = contentTable.editing? NO:YES;
    [contentTable setEditing:editing animated:true];
    
    if (editing == true)
    {
        [newFolderButton setHidden:false];
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [backButton setAlpha:0.0];
                             [newFolderButton setAlpha:1.0];
                             [currentFolder setFrame:CGRectMake(10, 0, 150, 50)];
                         } 
                         completion:^(BOOL finished){
                             //
                         }
         ];
    }
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationTransitionNone
                         animations:^{
                             [newFolderButton setAlpha:0.0];
                             [currentFolder setFrame:CGRectMake(60, 0, 150, 50)];
                             if ([pathToDir count] > 0) [backButton setAlpha:1.0];
                         } 
                         completion:^(BOOL finished){
                             [newFolderButton setHidden:true];
                         }
         ];
    }
}

- (IBAction)addNewFolder:(id)sender
{
    NSString * rootDir = [VKontakteWorks getRootDir];
    for (int i = 0; i < dirLevel; i++)
    {
        rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
    }
    NSString * localizedBaseNewFolderName = NSLocalizedString(@"Новая папка", nil);
    
    NSString * possiblyNewFolderName = [rootDir stringByAppendingPathComponent:localizedBaseNewFolderName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:possiblyNewFolderName] == false)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:possiblyNewFolderName withIntermediateDirectories:false attributes:nil error:nil];
    }
    else
    {
        NSMutableString * possiblyOtherNewFolderName = [[NSMutableString alloc] initWithString:possiblyNewFolderName];
        int j = 0;
        while ([[NSFileManager defaultManager] fileExistsAtPath:possiblyOtherNewFolderName] == true)
        {
            j++;
            [possiblyOtherNewFolderName setString:[NSString stringWithFormat:@"%@ %d", possiblyNewFolderName, j]];
        }
        [[NSFileManager defaultManager] createDirectoryAtPath:possiblyOtherNewFolderName withIntermediateDirectories:false attributes:nil error:nil];
        [possiblyOtherNewFolderName release];
    }
    
   [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
}

- (void)beginEditingFile:(id)sender event:(id)event
{
    NSString * editingFile = [[(UIButton *)sender titleLabel] text];
    
    NSSet * touches = [event allTouches];
    UITouch * touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:contentTable];
    NSIndexPath * indexPath = [contentTable indexPathForRowAtPoint:currentTouchPosition];
    
    if (indexPath == nil)
    {
        return;
    }
    
    [shared_filePath setString:editingFile];
    UIActionSheet * actions = [[UIActionSheet alloc] initWithTitle:[editingFile lastPathComponent] delegate:self cancelButtonTitle:NSLocalizedString(@"Отмена", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Переименовать", nil), NSLocalizedString(@"Копировать", nil), NSLocalizedString(@"Переместить", nil), NSLocalizedString(@"Удалить", nil), nil];
    [actions setTag:3];
    CGRect cellRect = [contentTable rectForRowAtIndexPath:indexPath];
    CGPoint yOffset = [contentTable contentOffset];
    [actions showFromRect:CGRectMake(cellRect.origin.x+964, cellRect.origin.y+50-yOffset.y, 60, 60) inView:self.view animated:true];
    [actions release];
}

- (void)didPresentAlertView:(UIAlertView *)alertView
{
    if (alertView.tag == 1)
    {
        [newNameField becomeFirstResponder];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString * rootDir = [VKontakteWorks getRootDir];
    for (int i = 0; i < dirLevel; i++)
    {
        rootDir = [rootDir stringByAppendingPathComponent:[pathToDir objectAtIndex:i]];
    }
    
    if (alertView.tag == 1) 
    {
        [newNameField resignFirstResponder];
        switch (buttonIndex) 
        {
            case 1:
                if ([newNameField.text length] < 1)
                {
                    [shared_filePath_Rename setString:shared_filePath];
                }
                else
                {
                    [shared_filePath_Rename setString:[rootDir stringByAppendingPathComponent:newNameField.text]];
                    /*NSDictionary * fileAttr = [[NSFileManager defaultManager] attributesOfItemAtPath:shared_filePath error:nil];
                    if (fileAttr != nil)
                    {
                        if ([fileAttr objectForKey:NSFileType] != NSFileTypeDirectory) 
                        {
                            [shared_filePath_Rename setString:[shared_filePath_Rename stringByAppendingPathExtension:[shared_filePath pathExtension]]];
                        }
                    }*/
                }
                [self performSelectorOnMainThread:@selector(tryToRenameFile) withObject:nil waitUntilDone:false];
                break;
            default:
                break;
        }
    }
}

- (void)tryToRenameFile
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:shared_filePath] == true)
    {
        BOOL uCannotUseThisFileName = false;
        if ([shared_filePath_Rename isEqualToString:shared_filePath] == true) uCannotUseThisFileName = true;
        if ([[[shared_filePath_Rename lastPathComponent] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 128) uCannotUseThisFileName = true;
        if (uCannotUseThisFileName == true) 
        {
            UIAlertView * errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil) message:NSLocalizedString(@"Вы не можете использовать указанное имя файла или папки", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"Готово", nil) otherButtonTitles:nil]; 
            [errorAlert show];
            [errorAlert release];
            return;
        }
        [[NSFileManager defaultManager] moveItemAtPath:shared_filePath toPath:shared_filePath_Rename error:nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"needUpdateStorage" object:nil];
}

- (IBAction)importPhotos:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType]; 
    [self presentModalViewController:picker animated:true];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)Picker 
{
    [Picker dismissModalViewControllerAnimated:true];
    [Picker release];
}

- (void)imagePickerController:(UIImagePickerController *)Picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        NSString * ext;
        NSString * type = [info objectForKey:UIImagePickerControllerMediaType];
        if ([type isEqualToString:@"public.movie"] == true)
        {
            ext = @"mov";
        }
        else
        {
            ext = @"png";
        }
        
        NSDate * currentDateTime = [NSDate date];
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy HH-mm-ss"];
        NSString * dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
        [dateFormatter release];
        
        NSString * saveDirImportedPhotos = [[VKontakteWorks getRootDir] stringByAppendingPathComponent:NSLocalizedString(@"Импортированные фото", nil)];
        NSString * name =  [NSString stringWithFormat:@"%@.%@", dateInStringFormated, ext];
        NSString * fileName = [saveDirImportedPhotos stringByAppendingPathComponent:name];
        
        if ([type isEqualToString:@"public.movie"] == true)
        {
            NSURL * videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            NSData * film = [NSData dataWithContentsOfURL:videoURL];
            [film writeToFile:fileName atomically:true];
        }
        else
        {
            UIImage * image = [info objectForKey:UIImagePickerControllerOriginalImage];
            [UIImagePNGRepresentation(image) writeToFile:fileName atomically:true];
        }
    }
    @catch (NSException * exception)
    {
        //
    }
    @finally
    {
        [Picker dismissModalViewControllerAnimated:true];
        [Picker release];
    }
}

@end