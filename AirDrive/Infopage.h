#import "GlobalImport.h"

@interface Infopage : UIViewController
{
    IBOutlet UILabel        * supportLabel;
    IBOutlet UILabel        * designLabel;
    IBOutlet UILabel        * versionLabel;
}

- (IBAction)closeInfoPage:(id)sender;

@end