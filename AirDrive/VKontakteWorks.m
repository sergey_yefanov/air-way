#import "VKontakteWorks.h"

@implementation VKontakteWorks

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag 
{
    NSLog(@"Audio finished playing.");
    [player release];
}

+ (NSString *)getRootDir
{
#if TARGET_IPHONE_SIMULATOR
    NSString * dir = @"/Users/Navern/Desktop";
#else
    NSString * dir = [NSString stringWithFormat:@"%@", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
#endif
    return dir;
}

+ (NSString *)getInboxDir
{
#if TARGET_IPHONE_SIMULATOR
    NSString * dir = @"/Users/Navern/Desktop/Inbox_test";
#else
    NSString * dir = [[NSString stringWithFormat:@"%@", [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]] stringByAppendingPathComponent:@"Inbox"];
#endif
    return dir;
}

/*+ (void)playSound:(int)effect
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    id test = [[NSUserDefaults standardUserDefaults] objectForKey:@"use_sounds"];
    if (test == nil)
    {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"use_sounds"];
    }
    BOOL use_sounds = [defaults boolForKey:@"use_sounds"];
    if (use_sounds == false)
    {
        return;
    }
    
    if (effect == 0) // панелька
    {
        SystemSoundID audioEffect;
        CFURLRef soundUrl = CFBundleCopyResourceURL(
                                                    CFBundleGetMainBundle(), 
                                                    CFSTR("panel"), 
                                                    CFSTR("mp3"), 
                                                    NULL
                                                    );
        AudioServicesCreateSystemSoundID(soundUrl, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    
    if (effect == 1) // коннект
    {
        SystemSoundID audioEffect;
        CFURLRef soundUrl = CFBundleCopyResourceURL(
                                                    CFBundleGetMainBundle(), 
                                                    CFSTR("connect"), 
                                                    CFSTR("mp3"), 
                                                    NULL
                                                    );
        AudioServicesCreateSystemSoundID(soundUrl, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    
    if (effect == 2) // дисконнект
    {
        SystemSoundID audioEffect;
        CFURLRef soundUrl = CFBundleCopyResourceURL(
                                                    CFBundleGetMainBundle(), 
                                                    CFSTR("disconnect"), 
                                                    CFSTR("caf"),
                                                    NULL
                                                    );
        AudioServicesCreateSystemSoundID(soundUrl, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    
    if (effect == 3) // ошибка
    {
        SystemSoundID audioEffect;
        CFURLRef soundUrl = CFBundleCopyResourceURL(
                                                    CFBundleGetMainBundle(), 
                                                    CFSTR("error"), 
                                                    CFSTR("wav"),
                                                    NULL
                                                    );
        AudioServicesCreateSystemSoundID(soundUrl, &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
}*/

@end