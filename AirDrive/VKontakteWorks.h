#import "GlobalImport.h"

@interface VKontakteWorks : NSObject

+ (NSString *)getRootDir;
+ (NSString *)getInboxDir;

@end