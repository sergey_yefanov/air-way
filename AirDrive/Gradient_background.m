#import "Gradient_background.h"

@implementation Gradient_background

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if ([self tag] == 1) // тень сверху-вниз
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGGradientRef myGradient;
        CGColorSpaceRef myColorSpace;
        size_t locationCount = 3;
        CGFloat locationList[] = {0.0, 0.2, 1.0};
        CGFloat colorList[] = 
        {
            100.0/255.0, 100.0/255.0, 100.0/255.0, 0.6,
            100.0/255.0, 100.0/255.0, 100.0/255.0, 0.2,
            100.0/255.0, 100.0/255.0, 100.0/255.0, 0.0
        };
        myColorSpace = CGColorSpaceCreateDeviceRGB();
        myGradient = CGGradientCreateWithColorComponents(myColorSpace, colorList, locationList, locationCount);
        
        CGPoint startPoint, endPoint;
        startPoint.x = 0;
        startPoint.y = 0;
        endPoint.x = 0;
        endPoint.y = CGRectGetMaxY(self.bounds);
        
        CGContextDrawLinearGradient(context, myGradient, startPoint, endPoint, 0);
    }
    else if ([self tag] == 2) // тень снзу-вверх
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGGradientRef myGradient;
        CGColorSpaceRef myColorSpace;
        size_t locationCount = 3;
        CGFloat locationList[] = {0.0, 0.8, 1.0};
        CGFloat colorList[] = 
        {
            100.0/255.0, 100.0/255.0, 100.0/255.0, 0.0,
            100.0/255.0, 100.0/255.0, 100.0/255.0, 0.2,
            100.0/255.0, 100.0/255.0, 100.0/255.0, 0.6
        };
        myColorSpace = CGColorSpaceCreateDeviceRGB();
        myGradient = CGGradientCreateWithColorComponents(myColorSpace, colorList, locationList, locationCount);
        
        CGPoint startPoint, endPoint;
        startPoint.x = 0;
        startPoint.y = 0;
        endPoint.x = 0;
        endPoint.y = CGRectGetMaxY(self.bounds);
        
        CGContextDrawLinearGradient(context, myGradient, startPoint, endPoint, 0);
    }
    else if ([self tag] == 3) // разделитель ячеек
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGGradientRef myGradient;
        CGColorSpaceRef myColorSpace;
        size_t locationCount = 2;
        CGFloat locationList[] = {0.0, 1.0};
        CGFloat colorList[] = 
        {
            204.0/255.0, 206.0/255.0, 209.0/255.0, 1.0,
            255.0/255.0, 255.0/255.0, 255.0/255.0, 1.0,
        };
        myColorSpace = CGColorSpaceCreateDeviceRGB();
        myGradient = CGGradientCreateWithColorComponents(myColorSpace, colorList, locationList, locationCount);
        
        CGPoint startPoint, endPoint;
        startPoint.x = 0;
        startPoint.y = 0;
        endPoint.x = 0;
        endPoint.y = CGRectGetMaxY(self.bounds);
        
        CGContextDrawLinearGradient(context, myGradient, startPoint, endPoint, 0);
    }
    else // градиент
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGGradientRef myGradient;
        CGColorSpaceRef myColorSpace;
        size_t locationCount = 2;
        CGFloat locationList[] = {0.0, 1.0};
        CGFloat colorList[] = 
        {
            245.0/255.0, 245.0/255.0, 250.0/255.0, 1.0,
            230.0/255.0, 231.0/255.0, 231.0/255.0, 1.0
        };
        myColorSpace = CGColorSpaceCreateDeviceRGB();
        myGradient = CGGradientCreateWithColorComponents(myColorSpace, colorList, locationList, locationCount);
        
        CGPoint startPoint, endPoint;
        startPoint.x = 0;
        startPoint.y = 0;
        endPoint.x = 0;
        endPoint.y = CGRectGetMaxY(self.bounds);
        
        CGContextDrawLinearGradient(context, myGradient, startPoint, endPoint, 0);
    }
}

@end